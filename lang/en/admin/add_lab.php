<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['machine'] = 'machine';
$string['machines'] = 'machines';
$string['createnewlab'] = 'Create new lab';
$string['computerlabs'] = 'Computer Labs';
$string['vieweditdetails'] = 'View/Edit Details';
$string['deletelab'] = 'Delete Lab';
$string['lowbandwidth'] = 'Low Bandwidth';
$string['name'] = 'Name';
$string['campus'] = 'Campus';
$string['building'] = 'Building';
$string['roomnumber'] = 'Room Number';
$string['bandwidth'] = 'Bandwidth';
$string['low'] = 'Low';
$string['high'] = 'High';
$string['timetabling'] = 'Timetabling';
$string['itsupport'] = 'IT Support';
$string['plagarism'] = 'Plagarism';
$string['ipaddresses'] = 'IP Addresses';
$string['save'] = 'Save';
$string['badaddressesinvalid'] = 'The following IP addresses are invalid: %s.';
$string['badaddressesinuse'] = 'The following IP addresses are already in use: %s.';
$string['backtolabs'] = 'Back to labs list';
$string['noipaddresses'] = 'Cannot create lab as there are no IP Addresses listed.';
$string['nolabname'] = 'Cannot create a lab with a blank name.';
$string['listcampuses'] = 'Campuses';
