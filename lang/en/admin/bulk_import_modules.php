<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/admin/list_modules.php';
require '../lang/' . $language . '/admin/add_module.php';

$string['entermoduleid'] = 'Please enter an Identifier for the module.';
$string['entermoduletitle'] = 'Please enter a title for the module.';
$string['moduleid'] = 'Module ID';
$string['intro'] = 'This function will allow new modules to be imported in bulk. Please enrole students to these modules separately.';
$string['mandatoryfields'] = 'Mandatory fields:';
$string['optionalfields'] = 'Optional fields:';
$string['csvfile'] = 'CSV File:';
$string['import'] = 'Import';
$string['alreadyexists'] = 'already exists';
$string['added'] = 'added';
$string['failed'] = 'failed';
$string['csvfileinvalid'] = 'CSV file invalid';
$string['csvfileloadfail'] = 'CSV failed to load';
$string['moduleid'] = 'Module code/identifier.';
$string['fullname'] = 'Module name.';
$string['school'] = 'Name of school running module.';
$string['smsapi'] = 'Student management system.';
$string['objectiveapi'] = 'Objective mapping system.';
$string['peerreview'] = 'Enable/Disable paper internal peer reviews summative exam check.';
$string['externalexaminers'] = 'Enable/Disable paper external examiner reviews summative exam check.';
$string['stdset'] = 'Enable/Disable standard setting summative exam check.';
$string['mapping'] = 'Enable/Disable Objective mapping summative exam check.';
$string['active'] = 'Activate/Deactivate module.';
$string['selfenrol'] = 'Enable/Disable student self enrolment onto module.';
$string['negmarking'] = 'Enable/Disable negative marking.';
$string['timedexams'] = 'Enable/Disable timed exams.';
$string['questionbasedfb'] = 'Enable/Disable question based feedback for students.';
$string['addteammember'] = 'Allow/Disallow module staff members to add additional members.';
$string['yearstart'] = 'Set academic year start for the module.';
$string['externalid'] = 'External system identifier for the module.';
$string['schoolcode'] = 'School Code. (used if school names not unique)';
