<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['allhotspots'] = 'Display all hotspot part areas';
$string['correctanswers'] = 'Correct answers';
$string['hotspots'] = 'Hotspots';
$string['incorrectanswers'] = 'Incorrect answers';
$string['viewall'] = 'View hotspots on all layers';
$string['addlayer'] = 'Add question part';
$string['removelayer'] = 'Delete question part';
$string['removelayerconfirm'] = "This will delete the active layer and all it's shapes.\n\nAre you sure you wish to continue?";
$string['move'] = 'Move shape';
$string['ellipse'] = 'Create ellipse';
$string['rectangle'] = 'Create rectangle';
$string['polygon'] = 'Create polygon';
$string['delete'] = 'Delete shape';
$string['help'] = 'Help';
$string['colourselect'] = 'Select a colour';
$string['cancel'] = 'Cancel';
