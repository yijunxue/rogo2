<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

if ($updater_utils->check_version('7.2.0')) {
    if (!$updater_utils->has_updated('rogo2673')) {
        $mediatypes = $configObject->get_setting('core', 'system_mediatypes');
        // Add mp4 as a media type.
        $mediatypes['mp4'] = 1;
        $configObject->set_setting('system_mediatypes', $mediatypes, Config::ASSOC);
        $updater_utils->record_update('rogo2673');
    }
}
